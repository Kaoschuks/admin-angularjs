"use strict";

angular
    .module('app.services')
    .factory("siteFunctions", siteFunctions)
    // decimal to hexadecinal
    function dec2hex (dec) 
    {
        return ('0' + dec.toString(16)).substr(-2)
    }

    // random csrf generator
    function generateId (len) 
    {
        var arr = new Uint8Array((len || 40) / 2)
        window.crypto.getRandomValues(arr)
        return Array.from(arr).map(dec2hex).join('')
    }

    function siteFunctions()
    {
        return {
            chartJS: function(id)
            {
                var myChart = new Chart(document.getElementById(id), {
                type: 'bar',
                data: {
                    labels: ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct'],
                    datasets: [{ 
                        data: [86,114,106,106,107,111,133,221,783,478],
                        label: "Page View",
                        backgroundColor: "#FFEB3B",
                        borderColor: "#FFEB3B",
                        fill: true
                    }, { 
                        data: [282,350,411,502,635,809,947,402,700,567],
                        label: "Google Likes",
                        backgroundColor: "#ff6b68",
                        borderColor: "#ff6b68",
                        fill: true
                    }, { 
                        data: [168,170,178,190,203,276,408,547,675,734],
                        label: "Visitors",
                        backgroundColor: "#3cba9f",
                        borderColor: "#3cba9f",
                        fill: true
                    }, { 
                        data: [740,520,810,616,624,38,74,167,508,784],
                        label: "Facebook Likes",
                        backgroundColor: "#673AB7",
                        borderColor: "#673AB7",
                        fill: true
                    }, { 
                        data: [6,3,2,2,7,26,82,172,312,433],
                        label: "Twitter Likes",
                        backgroundColor: "#2196F3",
                        borderColor: "#2196F3",
                        fill: true
                    }
                    ]
                },
                options: {
                    legendCallback: function (chart) {
                        var text = [];
                        text.push('<ul class="' + chart.id + '-legend" style="list-style:none">');
                        for (var i = 0; i < chart.legend.legendItems.length; i++) {
                            text.push('<li><div style="margin:5px !important;width:10px !important;height:10px !important;display:inline-block;background:' + chart.legend.legendItems[i].strokeStyle + '" />&nbsp;');
                            if (chart.legend.legendItems[i].text) {
                                text.push(chart.legend.legendItems[i].text);
                            }
                            text.push('</li>');
                        }
                        text.push('</ul>');
                
                        return text.join('');
                    },
                    legend: {display: false},
                    title: {
                        display: true,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
                });
                $("#myChartLegend").html(myChart.generateLegend()); 
            },
            dataTable: function(tableName)
            {
                $('#data-table_filter label input').attr('placeholder', 'Search datagrid by name or title');
                $(tableName).DataTable({
                    responsive: true
                });
            },
            wysihtml5: function()
            {
                $('textarea').wysihtml5();
            },
            readURL: function(input, displayId) 
            {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function(e) 
                    {
                        $('#'+ displayId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            },
            uploadFiles: function($rootScope)
            {
                $('button.btn-upload').removeClass('btn-upload');
                $('button.btn-upload').css('margin: 20px !important');
            }
        }
    }