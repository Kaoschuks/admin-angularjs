
var templateURL = './';
var uploadURL = '//localhost:8888/authenticdoc/backend/model/uploads/';
var lock = true;

(function() {
    if (!sessionStorage.length) {
        // Ask other tabs for session storage
        localStorage.setItem('getSessionStorage', Date.now());
    };
    window.addEventListener('storage', function(event) {
        if (event.key == 'getSessionStorage') {
            localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
            localStorage.removeItem('sessionStorage');

        } else if (event.key == 'sessionStorage' && !sessionStorage.length) {
            var data = JSON.parse(event.newValue),
                        value;

            for (key in data) {
                sessionStorage.setItem(key, data[key]);
            }
        }
    });
    window.onbeforeunload = function() {
        //sessionStorage.clear();
    };

})();

function cutString(text, len)
{    
    text = $($.parseHTML(text)).text();
    var i = 0;
    var wordsToCut = len;
    var wordsArray = text.split(" ");
    if(wordsArray.length>wordsToCut){
        var strShort = "";
        for(i = 0; i < wordsToCut; i++){
            strShort += wordsArray[i] + " ";
        }   
        return strShort+"...";
    }else{
        return text;
    }
 }

function collapselock()
{
    if(sessionStorage.getItem('collapselock') === false)
    {
        sessionStorage.setItem('collapselock', true);
    }
    return sessionStorage.getItem('collapselock');
}

function filesManager(module, BasicService, $rootScope)
{
    $('#selectImage').on('click', function(){
        BasicService.GetData(module+"Files", "", function(response)
        {
            if (response.status == 'OK') {
                $rootScope.Files = response.output;
                $('#galleryBtn').click();
            } else {
                $rootScope.Error = response.output;
            }
        });
    });
}

function saveImageUrl(url)
{
    sessionStorage.setItem('image', url);
    $('button.close').click();
    $('img#featuredImage').attr('src', url);
    $('img#featuredImage').removeClass('hidden');
}

// this transforms any form data into proper encoding for ajax communication
function transformRequest(obj)
{
    var $res = [];
    for(var key in obj)
    {
        $res.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return $res.join('&');
}

function clickUpload()
{
    $('input#file').click();
}

function clickErrPopup()
{
    $('button#errorBtn').click();
}

function responseNotify(type, title, msg)
{
    switch(type)
    {
        case "Error":
        {
            $.notify({
                title: title, 
                type: 'danger', 
                message: '<strong>Oops</strong> '+msg+'. Contact tech support', 
                progress: 20 
            });
            break;
        }
        case "Success":
        {
            $.notify({ 
                title: title,
                type: 'success', 
                message: '<strong>Success</strong> '+msg+'.', 
                progress: 20 
            });
            break;
        }
    }
}

function clickOkPopup()
{
    $('button#okBtn').click();
}

function checkVariable(elem) {
    var type = typeof elem;
    switch (type) {
        case "string":
            {
                return "isString";
                break;
            }
        case "number":
            {
                return "isNumber";
                break;
            }
        case "object":
            {
                return "isObject";
                break;
            }
    }
}

function aContainsB(a, b) {
    return a.indexOf(b) >= 0;
}
