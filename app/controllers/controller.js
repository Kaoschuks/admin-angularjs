"use strict";

angular
    .module('app.controllers', [])
    .controller("mainCtrl", mainController)
    .controller("errCtrl", errorController);

function mainController($scope, modules, $state, localStorageService, $rootScope, BasicService, $location, siteFunctions) 
{
    var app = this;     
    $scope.user = $rootScope.globals.currentUser.username;
    app.DashboardData = {};
    
    (function initController() {
        // get all user data
        switch($state.current.name)
        {
            case "Dashboard":
            {
                app.DashboardData['Username'] = $rootScope.globals.currentUser.username;
                getDashboardData();
                // siteFunctions.chartJS("line-chart");
                break;
            }
            case "Seo":
            {
                break;
            }
            case "Logs":
            {
                BasicService.GetData("Analytics", "/Logs", function(response)
                {
                    if (response.status == 'OK') {
                        app.serverLogs = {} = response.Output;
                        $rootScope.dataLoading = false;
                    } else {
                        $scope.Error = response.Output;
                        $rootScope.dataLoading = false;
                    }
                });
                // siteFunctions.chartJS("line-chart");
                break;
            }
            case "Lockscreen":
            {
                $rootScope.globals.currentUser.authData = null;
                window.location.href = "Lockscreen";
                break;
            }
        }
    })();

    function getDashboardData()
    {
        $rootScope.dataLoading = true;
    }
};

function errorController($scope, $state, $location)
{
    (function initController() {
        //$location.path('Login');
    })();
    
    function proccessErrPage(location)
    {
        switch(location)
        {
            case "/404":
            {
                $scope.pageDescription = "Page not found";
                break;
            }
            case "/500":
            {
                $scope.pageDescription = "Internal server error";
                break;
            }
        }
    }
};
